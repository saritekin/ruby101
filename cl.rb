class Human
  attr_accessor :name, :surname, :gender
  def initialize(name, surname, gender)
    @name = name
    @surname = surname
    @gender = gender
  end

  def breath
    puts "#{@name} can breath"
  end
end

serhat = Human.new('Serhat', 'Doğruyol', 'E')
puts serhat.name
serhat.name = "denoo"
puts serhat.name
serhat.breath
