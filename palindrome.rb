def palindrome?(str)
  str.upcase!.gsub!(/\W/, '')
  str == str.reverse
end

puts(palindrome?('tRaş Ni Çin şart'))
