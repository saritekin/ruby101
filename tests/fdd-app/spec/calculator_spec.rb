require "Calculator"

RSpec.describe Calculator do
  
  context "#topla" do

    it "returns sum of number 1 and number 2" do
      calculator = Calculator.new
      expect(calculator.topla(3,5)).to eq 8
    end
    it "carp" do
       calculator = Calculator.new
       expect(calculator.carp(3,5)).to eq 15
    end
    it "cikar" do
        calculator = Calculator.new
        expect(calculator.cikar(3,5)).to eq -2
    end
    it "bol" do
        calculator = Calculator.new
        expect(calculator.bol(6,3)).to eq 2
    end
    it "AtAtAt" do
       calculator = Calculator.new
       expect { calculator.topla("At",5)}.to raise_error(NotANumberError, "Sayı Giriniz")
    end
  end
end
