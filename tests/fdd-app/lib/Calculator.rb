class NotANumberError < StandardError
end

class Calculator
  HATA_MESAJI = "Sayı Giriniz"
  def topla(s1, s2)
    fail NotANumberError, HATA_MESAJI unless [s1, s2].all? { |s| s.is_a?(Fixnum) }
      s1 + s2
  end
  def carp(s1, s2)
    fail NotANumberError, HATA_MESAJI unless [s1, s2].all? { |s| s.is_a?(Fixnum) }
      s1 * s2
  end
  def bol(s1, s2)
    fail NotANumberError, HATA_MESAJI unless [s1, s2].all? { |s| s.is_a?(Fixnum) }
      s1 / s2
  end
  def cikar(s1, s2)
    fail NotANumberError, HATA_MESAJI unless [s1, s2].all? { |s| s.is_a?(Fixnum) }
      s1 - s2
  end
end

