def topla(s1, s2)
  raise ArgumentError, 'Parametreler sayı olmalı'\
  unless [s1, s2].all? { |s| s.is_a? Numeric }
  s1 + s2
end

puts(topla(3, 5))
puts(topla(3, 'hi'))
