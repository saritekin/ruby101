def hesapla(s1, s2)
  block_given? ? yield(s1, s2) : s1 + s2
end

hesapla(4, 5) { |a, b| a - b }
